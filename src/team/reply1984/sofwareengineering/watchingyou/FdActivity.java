package team.reply1984.sofwareengineering.watchingyou;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.io.*;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2;

import team.reply1984.sofwareengineering.watchingyou.R;

import org.opencv.highgui.*;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.StrictMode;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.ProgressBar;
import android.widget.TextView;

@SuppressLint("SimpleDateFormat")
public class FdActivity extends Activity implements CvCameraViewListener2, OnClickListener {

	private static String 			logName;		//the name of a log is specific date of that day ex) 20131127.txt
	private static File				logFile;		//file pointer to write  a log
    private static final String    TAG                 = "OCVSample::Activity";		//just some tag for keeping tracks
    private static final Scalar    FACE_RECT_COLOR     = new Scalar(0, 255, 0, 255);	//the color of the rect that covers the face
    public static final int        NATIVE_DETECTOR     = 1;		//just some flag to keep tracks


    private Mat                    mRgba;			//a matrix of colors generated from camera feeds	
    private Mat                    mGray;			//a matrix of mono tone for face detection
    private File                   mCascadeFile;	//this pointer points to the file which holds pre-defined informations to detect faces
    private DetectionBasedTracker  mNativeDetector; //a class for invoking face detection written in native C++

    private String					mDetectorName;	//just some var to keep tracks of codes

    private float                  mRelativeFaceSize   = 0.2f;	//initial relative face(the rect) factor
    private int                    mAbsoluteFaceSize   = 0;		//initial size of the face(the rect)

    private CameraBridgeViewBase   mOpenCvCameraView;	//a view and also a thread that calls for camera feed
    
    private static String			inputString;	//the string via which mastercode is streamed 
    private long 					noFaceFrom;		//the start time of the first alert
    private long 					leftTime;		//time left before the alarm goes off
    private boolean 				firstWarn;		//is first warn fired?
    private boolean 				firstWarn_old;	//has first warn fired?	for warn should be called only once
    private boolean					secondWarn;		//is second warn fired?
    private boolean					secondWarn_old;	//has second warn fired? for warn should be called only once
    private ProgressBar				progressCircle;	//just some circle to make the app "it"
    private TextView				alertText; 		//this holds "face detection needed to clear the alarm"
    private long					timeCreated;	//time at which this activity started
    public static Toast 			mToast;			//object for making toast
    private static AudioManager		mAudioManager;	//object for setting alarm volume
    private static Ringtone			alarmRingtone;	//object for playing the alarm
    private static GMailSender mailSender;			//object for sending mail of any kind
    //this file pointer points to dir where the photo will be saved
    private static File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "WatchingYou");
    private static Boolean	openCVLoaded = false;	//has library loaded?
    /*
     * a method for making numberpads invisible
     */
    private void number_pad_invisible() {
    	findViewById(R.id.button1).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button2).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button3).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button4).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button5).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button6).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button7).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button8).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button9).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button10).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button11).setVisibility(LinearLayout.INVISIBLE);
        findViewById(R.id.button12).setVisibility(LinearLayout.INVISIBLE);
    }
    /*
     * a method for making numberpads visible
     */
    private void number_pad_visible() {
    	findViewById(R.id.button1).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button2).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button3).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button4).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button5).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button6).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button7).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button8).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button9).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button10).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button11).setVisibility(LinearLayout.VISIBLE);
        findViewById(R.id.button12).setVisibility(LinearLayout.VISIBLE);
    }
    /*
     * a method to go back to MainActivity
     */
    private void returnMainActivity() {
    	startActivity(new Intent(FdActivity.this, MainActivity.class));
		overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
		finish();
    }
    
    /*
     * a method that overrides finish to make sure alarm is cleared
     */
    @Override
    public void finish() {
    	firstWarn=false;
    	firstWarn_old=false;
    	secondWarn=false;
    	secondWarn_old=false;
    	alarmRingtone.stop();
    	super.finish();
    }
    
    /**
     * a method for sending SMS
     * @param phoneNumber
     * @param message
     */
    private void sendSMS(String phoneNumber, String message){
    	
    	String SENT = "SMS_SENT";
    	String DELIVERED = "SMS_DELIVERED";
     	

        // 문자 보내는 상태를 감지하는 PendingIntent
    	PendingIntent sentPI = PendingIntent.getBroadcast(this, 0, new Intent(SENT), 0);
        // 문자 받은 상태를 감지하는 PendingIntent
    	PendingIntent deliveredPI = PendingIntent.getBroadcast(this, 0, new Intent(DELIVERED), 0);
       	

        // 문자 보내는 상태를 감지하는 BroadcastReceiver를 등록한다.
    	registerReceiver(new BroadcastReceiver() {		

                       // 문자를 수신하면, 발생. 
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					//Toast.makeText(getBaseContext(), "SMS sent", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
					Toast.makeText(getBaseContext(), "Generic failure", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NO_SERVICE:
					Toast.makeText(getBaseContext(), "No service", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_NULL_PDU:
					Toast.makeText(getBaseContext(), "Null PDU", Toast.LENGTH_SHORT).show();
					break;
				case SmsManager.RESULT_ERROR_RADIO_OFF:
					Toast.makeText(getBaseContext(), "Radio off", Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(SENT));
    	
    	// 문자를 받는 상태를 확인하는 BroadcastReceiver를 등록.
    	registerReceiver(new BroadcastReceiver() {
			

                        // 문자를 받게 되면, 불린다. 
			@Override
			public void onReceive(Context context, Intent intent) {
				switch (getResultCode()) {
				case Activity.RESULT_OK:
					Toast.makeText(getBaseContext(), "SMS delivered", Toast.LENGTH_SHORT).show();
					break;
				case Activity.RESULT_CANCELED:
					Toast.makeText(getBaseContext(), "SMS not delivered", Toast.LENGTH_SHORT).show();
					break;
				}
			}
		}, new IntentFilter(DELIVERED));
     	

        // SmsManager를 가져온다.
    	SmsManager sms = SmsManager.getDefault();
        // sms를 보낸다. 
    	sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
    }
    /*
     * a static code part for loading OpenCV
     */
    static {
    	openCVLoaded = OpenCVLoader.initDebug();
    }
    /*
     * a method for defining call back function for manipulating camera feeds
     */
    private BaseLoaderCallback  mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS:
                {
                    Log.i(TAG, "OpenCV loaded successfully");

                    // Load native library after(!) OpenCV initialization
                    System.loadLibrary("detection_based_tracker");

                    try {
                        // load cascade file from application resources
                        InputStream is = getResources().openRawResource(R.raw.lbpcascade_frontalface);
                        File cascadeDir = getDir("cascade", Context.MODE_PRIVATE);
                        mCascadeFile = new File(cascadeDir, "lbpcascade_frontalface.xml");
                        FileOutputStream os = new FileOutputStream(mCascadeFile);

                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = is.read(buffer)) != -1) {
                            os.write(buffer, 0, bytesRead);
                        }
                        is.close();
                        os.close();

                        mNativeDetector = new DetectionBasedTracker(mCascadeFile.getAbsolutePath(), 0);

                        cascadeDir.delete();

                    } catch (IOException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Failed to load cascade. Exception thrown: " + e);
                    }

                    mOpenCvCameraView.enableView();
                } break;
                
                default: super.onManagerConnected(status); break;
            }
        }
    };
    
    /*
     * a creator that set some flags and loading OpenCV library
     */
    public FdActivity() {
        mDetectorName  = "Native (tracking)";
        Log.i(TAG, "Instantiated new " + this.getClass() + " as " + mDetectorName);
        openCVLoaded = OpenCVLoader.initDebug();
    }
    
    /*
     * a method called on creation life cycle of application
     * initialize vars like the time, file path, volume, view etc
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	StrictMode.ThreadPolicy policy 
    		= new StrictMode.ThreadPolicy.Builder().permitAll().build();
    	StrictMode.setThreadPolicy(policy);
    	logName = new SimpleDateFormat("yyyyMMdd").format(new Date())+".txt";
    	logFile = new File("logName");
    	if(!logFile.exists())
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	new Handler();
    	mAudioManager = (AudioManager)getSystemService(AUDIO_SERVICE);
    	int maxVolume = mAudioManager.getStreamMaxVolume(AudioManager.STREAM_RING);
    	mAudioManager.setStreamVolume(AudioManager.STREAM_ALARM, maxVolume, 0);
    	new RingtoneManager(FdActivity.this);
    	alarmRingtone = RingtoneManager.getRingtone(FdActivity.this, RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE));
    	alarmRingtone.setStreamType(AudioManager.STREAM_ALARM);
    	noFaceFrom = System.currentTimeMillis();
    	firstWarn = false;
    	firstWarn_old = false;
    	secondWarn = false;
    	secondWarn_old = false;
    	leftTime = 10000;
    	inputString="";
    	timeCreated = System.currentTimeMillis();
    	mailSender = new GMailSender();
    	
        Log.i(TAG, "called onCreate");
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        try {
			startAlert();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        setContentView(R.layout.face_detect_surface_view);
        mOpenCvCameraView = (CameraBridgeViewBase) findViewById(R.id.fd_activity_surface_view);
        mOpenCvCameraView.setCvCameraViewListener(this);
        mOpenCvCameraView.setCameraIndex(CameraBridgeViewBase.CAMERA_ID_FRONT);
        
        LayoutInflater inflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout linear = (LinearLayout)inflater.inflate(R.layout.over, null);
        @SuppressWarnings("deprecation")
		LinearLayout.LayoutParams paramlinear 
        	= new LinearLayout.LayoutParams( 
        		LinearLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.FILL_PARENT
        	);
        getWindow().addContentView(linear, paramlinear);
        
        LayoutInflater inflater1 = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        RelativeLayout linear1 = (RelativeLayout)inflater1.inflate(R.layout.num_pad, null);
        @SuppressWarnings("deprecation")
		RelativeLayout.LayoutParams paramlinear1 
        	= new RelativeLayout.LayoutParams( 
        			RelativeLayout.LayoutParams.FILL_PARENT,LinearLayout.LayoutParams.FILL_PARENT
        	);
        getWindow().addContentView(linear1, paramlinear1);
         
        findViewById(R.id.end_button).setOnClickListener(this);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
        findViewById(R.id.button3).setOnClickListener(this);
        findViewById(R.id.button4).setOnClickListener(this);
        findViewById(R.id.button5).setOnClickListener(this);
        findViewById(R.id.button6).setOnClickListener(this);
        findViewById(R.id.button7).setOnClickListener(this);
        findViewById(R.id.button8).setOnClickListener(this);
        findViewById(R.id.button9).setOnClickListener(this);
        findViewById(R.id.button10).setOnClickListener(this);
        findViewById(R.id.button11).setOnClickListener(this);
        findViewById(R.id.button12).setOnClickListener(this);
        number_pad_invisible();
        
      //------------------------------------------------------------------------
    	progressCircle = (ProgressBar) findViewById(R.id.progressCircle);
        progressCircle.setVisibility(ProgressBar.INVISIBLE);

        alertText = (TextView) findViewById(R.id.AlertText);
        alertText.setVisibility(TextView.INVISIBLE);
        
        mToast = Toast.makeText(this, "", Toast.LENGTH_LONG);    
    }
    
    /*
     * a method called when numberpads are clicked
     * stream inputs of numbers for the mastercode
     * if mastecode is correct, it calls for finish() or just warn that it is wrong
     */
	@Override
    public void onClick(View v)
    {
        switch(v.getId())
        {
        	case R.id.end_button: 
        		Toast.makeText(FdActivity.this, "비밀번호를 입력하세요",Toast.LENGTH_SHORT).show();
        		number_pad_visible();
        		inputString = "";
        	break;
        	case R.id.button1: //enter
        		if(inputString.equals(SettingManager.master_code)) returnMainActivity();
        		else {
        			Toast.makeText(FdActivity.this, "비밀번호가 맞지 않습니다",Toast.LENGTH_SHORT).show();
        			number_pad_invisible();
        			inputString = "";
        		}
            break; 
        	case R.id.button2: //delete
        		if(inputString.length()>=2)
        			inputString = inputString.substring(0, inputString.length()-1);
        		else if(inputString.length()>=1)
        			inputString = "";
        		else ;
            	break;
        	case R.id.button3: //0
        		inputString += "0";
            	break;
        	case R.id.button4: //3
        		inputString += "3";
            	break;
        	case R.id.button5: //2
        		inputString += "2";
            	break;
        	case R.id.button6: //1
        		inputString += "1";
            	break;
        	case R.id.button7: //6
        		inputString += "6";
            	break;
        	case R.id.button8: //5
        		inputString += "5";
            	break;
        	case R.id.button9: //4
        		inputString += "4";
            	break;
        	case R.id.button10: //9
        		inputString += "9";
            	break;
        	case R.id.button11: //8
        		inputString += "8";
            	break;
        	case R.id.button12: //7
        		inputString += "7";
            	break;
        }
        if(inputString.length()==8) {
        	if(inputString.equals(SettingManager.master_code)) returnMainActivity();
        	else {
        		Toast.makeText(FdActivity.this, "비밀번호가 맞지 않습니다",Toast.LENGTH_SHORT).show();
        		number_pad_invisible();
        		inputString = "";
        	}
        }
    }
    
	/*
     * a method called on pausing life cycle of application
     * disable camera view
     */
    @Override
    public void onPause()
    {
    	try {
			pauseAlert();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        super.onPause();
        if (mOpenCvCameraView != null)
            mOpenCvCameraView.disableView();
    }

    /*
     * a method called on resuming life cycle of application
     * calls for call back function for camerafeeds
     */
    @Override
    public void onResume()
    {
    	try {
			resumeAlert();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        super.onResume();
        //OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_3, this, mLoaderCallback); 
        if(openCVLoaded) mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
    }

    /*
     * a method called on destruction life cycle of application
     * disable camera view
     */
    public void onDestroy() {
    	try {
			endAlert();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        super.onDestroy();
        mOpenCvCameraView.disableView();
    }

    /*
  	 * camera view started
     */
    public void onCameraViewStarted(int width, int height) {
        mGray = new Mat();
        mRgba = new Mat();
    }

    /*
     * camera view stopped
     */
    public void onCameraViewStopped() {
        mGray.release();
        mRgba.release();
    }

    /*
     * actual method that manipulates camera view
     * camera view is retrieved then converted to mat form for OpenCV manipulation
     * after that that mat var is sent to native C++ face detection codes
     * and returns arrays of coordinates of faces detected
     * if there is some, continue
     * else there are none, it gives use 10 secs to comeback and flashes the screen this is the first warn
     * if first warn is ignored, the second warn goes off
     * the camera view of the moment when the second warn started is saved and sent to observer
     * also the actual alarms goes off very loudly
     * if face is shown again, it goes back to normal state
     * this cycle is repeated till mastercode is input and finishes
     */
    public Mat onCameraFrame(CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        mGray = inputFrame.gray();

        if (mAbsoluteFaceSize == 0) {
            int height = mGray.rows();
            if (Math.round(height * mRelativeFaceSize) > 0) {
                mAbsoluteFaceSize = Math.round(height * mRelativeFaceSize);
            }
            mNativeDetector.setMinFaceSize(mAbsoluteFaceSize);
        }

        MatOfRect faces = new MatOfRect();

        mNativeDetector.detect(mGray, faces);

        Rect[] facesArray = faces.toArray();
        for (int i = 0; i < facesArray.length; i++)
            Core.rectangle(mRgba, facesArray[i].tl(), facesArray[i].br(), FACE_RECT_COLOR, 3);
        
        Core.flip(mRgba, mRgba, 1);
        Mat temp = mRgba.clone();
        Core.putText(mRgba, inputString, new Point(10,mRgba.height()-10), Core.FONT_HERSHEY_TRIPLEX, 5.0, FACE_RECT_COLOR);
        if((System.currentTimeMillis()-timeCreated < 7000)) {
        	long probationLeft = 7000 - (System.currentTimeMillis()-timeCreated);
        	String s = "Starts in "+Integer.toString((int) (probationLeft/1000)) + "." + Integer.toString((int) (probationLeft%1000/100)) + " sec";
        	Core.putText(mRgba, s, new Point(mRgba.width()/2-400, mRgba.height()/2+50), Core.FONT_HERSHEY_TRIPLEX, 3.0, FACE_RECT_COLOR);
        	
        } else if(facesArray.length==0) {
        	Core.putText(mRgba, "No Face!", new Point(mRgba.width()-460,80), Core.FONT_HERSHEY_TRIPLEX, 3.0, FACE_RECT_COLOR);
        	if(firstWarn==false && firstWarn_old==false && secondWarn==false && secondWarn_old==false) {
        		noFaceFrom = System.currentTimeMillis();
        		firstWarn = true;
        	} else if (firstWarn==true && firstWarn_old==false && secondWarn==false && secondWarn_old==false) {
        		firstWarn_old = true;
        		firstAlert();
        	} else if (firstWarn==true && firstWarn_old==true && secondWarn==false && secondWarn_old==false) {
        		leftTime = 10000 - (System.currentTimeMillis() - noFaceFrom);
        		if(0<leftTime) {
        			String s = Integer.toString((int) (leftTime/1000)) + "." + Integer.toString((int) (leftTime%1000));
        			Core.putText(mRgba, s, new Point(mRgba.width()/2-100, mRgba.height()/2+50), Core.FONT_HERSHEY_TRIPLEX, 3.0, FACE_RECT_COLOR);
        			if(leftTime%2 ==1) mRgba.convertTo(	mRgba, mRgba.depth(), -1, 255);
        		} else secondWarn = true;
        	} else if (firstWarn==true && firstWarn_old==true && secondWarn==true && secondWarn_old==false) {
        		if(!mediaStorageDir.exists()) new File(mediaStorageDir.getPath()).mkdirs();
        		File file = new File(mediaStorageDir.getPath(), new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date()) + ".png");
        		Highgui.imwrite(file.toString(), temp);
        		secondWarn_old = true;
        		try {
					secondAlert(file.getPath(), file.getName());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
        	} else if (firstWarn==true && firstWarn_old==true && secondWarn==true && secondWarn_old==true) {
        		runOnUiThread(new Runnable() {
            		@Override
            		public void run() {	
            			if (firstWarn==true && firstWarn_old==true && secondWarn==true && secondWarn_old==true)
            				if(!alarmRingtone.isPlaying()) alarmRingtone.play();
            		}
            	});
        	}
        }
        else {
        	if (firstWarn==true && firstWarn_old==true && secondWarn==true && secondWarn_old==true) {
        		firstWarn = false;
	        	firstWarn_old = false;
	        	secondWarn = false;
	        	secondWarn_old = false;
	        	runOnUiThread(new Runnable() {
	        		@Override
	        		public void run() {
	        			progressCircle.setVisibility(ProgressBar.INVISIBLE);
	        			alertText.setVisibility(TextView.INVISIBLE);
	        			alarmRingtone.stop();
	        		}
	        	});
        	}
        	noFaceFrom = System.currentTimeMillis();
        }
        temp.release();
        return mRgba;
    }
    /*
     * sends message to the observer that face detection watching has stared via SMS and mail
     */
    public void startAlert() throws Exception {
    	sendSMS(SettingManager.phone, "Watching You가 관찰을 시작합니다");
    	makeLog("Watching You가 관찰을 시작합니다");
    	new Thread( new Runnable() {
				@Override
				public void run() {
					mailSender.sendMail("Watching You가 전해드립니다", 
		    			"Watching You가 관찰을 시작합니다");
					/*mailSender.sendMailWithFile("Watching You가 전해드립니다", 
			    			 "귀댁의 자녀 "+SettingManager.name+"분은 지금, 하라는 공부는 안하고!",
			    			"/storage/sdcard0/Naver/downloadfile-14.jpeg",
			    			"dingding.jpeg");*/
				}
			}
    	).start();
    	
    }
    /*
     * sends message to the observer that face detection watching has paused via SMS and mail
     */
    public void pauseAlert() throws IOException {
    	sendSMS(SettingManager.phone, "Watching You가 관찰을 중지합니다");
    	makeLog("Watching You가 관찰을 중지합니다");
    	new Thread( new Runnable() {
			@Override
			public void run() {
				mailSender.sendMail("Watching You가 전해드립니다", 
		    			"Watching You가 관찰을 중지합니다");
			}
		}
	).start();
    }
    /*
     * sends message to the observer that face detection watching has resumed via SMS and mail
     */
    public void resumeAlert() throws IOException {
    	sendSMS(SettingManager.phone, "Watching You가 관찰을 재개합니다");
    	makeLog("Watching You가 관찰을 재개합니다");
    	new Thread( new Runnable() {
			@Override
			public void run() {
				mailSender.sendMail("Watching You가 전해드립니다", 
		    			"Watching You가 관찰을 재개합니다");
			}
		}
	).start();
    }
    /*
     * sends message to the observer that face detection watching has ended via SMS and mail
     */
    public void endAlert() throws IOException {
    	sendSMS(SettingManager.phone, "Watching You가 관찰을 종료합니다");
    	makeLog("Watching You가 관찰을 종료합니다");
    	new Thread( new Runnable() {
			@Override
			public void run() {
				mailSender.sendMail("Watching You가 전해드립니다", 
		    			"Watching You가 관찰을 종료합니다");
			}
		}
	).start();
    }
    /*
     * just method to make toast for the first warn
     */
    public void firstAlert() {
    	makeToast("1차 경고입니다",Toast.LENGTH_SHORT);
    }
    /*
     * a thread method to invoke sending message via gmail
     */
    public class CustomThread extends Thread { 
		private String value; 
		private String anotherValue; 
		public CustomThread(String value, String anotherValue) { 
			this.value = value; 
			this.anotherValue = anotherValue; 
		} 
		public void run() {
			/*mailSender.sendMailWithFile("Watching You가 전해드립니다", 
	    			 "귀댁의 자녀 "+SettingManager.name+"분은 지금, 하라는 공부는 안하고!",
	    			"/storage/sdcard0/Naver/downloadfile-14.jpeg",
	    			"dingding.jpeg");*/
			mailSender.sendMailWithFile("Watching You가 전해드립니다", 
	    			 "귀댁의 자녀 "+SettingManager.name+"분은 지금, 하라는 공부는 안하고!",
	    			value, anotherValue);
		}
	}
    /*
     * a method for second warn that sends SMS, gmail, and make toast
     */
    public void secondAlert(String filePath, String fileName) throws IOException {
    	makeToast("2차 경고입니다",Toast.LENGTH_SHORT);
    	sendSMS(SettingManager.phone, "귀댁의 자녀 "+SettingManager.name+"분은 지금, 하라는 공부는 안하고!");
    	makeLog("귀댁의 자녀 "+SettingManager.name+"분은 지금, 하라는 공부는 안하고!");
    	new CustomThread(filePath, fileName).start();

    	//Camera thread !== UI thread
    	runOnUiThread(new Runnable() {
    		@Override
    		public void run() {
    			progressCircle.setVisibility(ProgressBar.VISIBLE);
    			alertText.setVisibility(TextView.VISIBLE);    			
    			alarmRingtone.play();
    		}
    	});
    }
    
    /*
     * a method to disable all possible hardware key input
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
    	mOpenCvCameraView.cancelLongPress();
    	return true;
    }
    /*
     * a method for making toast
     */
    private void makeToast(final String text, final int duration) {
        mToast.cancel();
        mToast.setText(text);
        mToast.setDuration(duration);
        mToast.show();
    }
    /*
     * a method for writing logs
     */
    private void makeLog(String text) throws IOException {
    	FileOutputStream fos = openFileOutput(logName,MODE_APPEND);
    	text = new SimpleDateFormat("dd/HH:mm:ss ").format(new Date()).toString() + text +"\n";
    	PrintWriter out = new PrintWriter(fos);
    	out.print(text);
    	out.close();
    	fos.close();
    }
    
}