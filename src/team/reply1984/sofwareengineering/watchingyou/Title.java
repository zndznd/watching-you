package team.reply1984.sofwareengineering.watchingyou;

import team.reply1984.sofwareengineering.watchingyou.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

//타이틀 이미지를 보여주기 위한 화면
public class Title extends Activity {

	//애니메이션 효과의 값을 설정하기 위한 변수
	static final int DELAY_TIME = 3000;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.title);
		
		SettingManager manager = new SettingManager(this);
		//설정 파일이 존재할 시에는 MainActivity로 전환
		if(manager.getExist()){
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {		
					startActivity(new Intent(Title.this, MainActivity.class));
					overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
					finish();
				}
			}, DELAY_TIME);
		}

		//설정 파일이 존재하지 않을 시에는 설정 파일을 만들기 위해 SettingActivity로 전환한다.
		else{
			new Handler().postDelayed(new Runnable() {
				@Override
				public void run() {		
					startActivity(new Intent(Title.this, Setting_Activity.class));
					overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
					finish();
				}
			}, DELAY_TIME);
		}
	}
}
