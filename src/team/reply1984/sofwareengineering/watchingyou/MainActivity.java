package team.reply1984.sofwareengineering.watchingyou;

import team.reply1984.sofwareengineering.watchingyou.R;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.text.Editable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

//주된 메뉴를 나타내는 화면
public class MainActivity extends Activity {

	//애니메이션 효과의 값을 설정하기 위한 변수
	protected static final int DELAY_TIME = 100;  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
	}
	
	public void startFaceDetection(View v) throws NameNotFoundException
	{
		//FdActivity로 화면을 전환
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				startActivity(new Intent(MainActivity.this,FdActivity.class));
				//화면전환 애니메이션 효과 코드
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				//현재 액티비티를 종료
				finish();
			}
		}, DELAY_TIME);
	}
	
	public void startSetting(View v)throws NameNotFoundException{
		//대화 상자를 띄워서 마스터 코드와 일치하는 숫자를 입력했을 시에만 설정 변경 액티비티로 전환한다.
		Context mContext = getApplicationContext();
		LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(LAYOUT_INFLATER_SERVICE);
		View layout = inflater.inflate(R.layout.custom_dialog, (ViewGroup)findViewById(R.id.layout_root));
		
		//대화 상자의 속성 및 행동을 설정
		AlertDialog.Builder aDialog = new AlertDialog.Builder(MainActivity.this);
		aDialog.setTitle("설정을 수정하시겠습니까?");
		aDialog.setView(layout);
								
		aDialog.setPositiveButton("로그인", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				AlertDialog tempAlertDialog = (AlertDialog) dialog;
				View	tempView = tempAlertDialog.findViewById(R.id.master_enter);
				EditText tempEditText = (EditText)tempView;
				Editable	tempEditable = tempEditText.getText();
				String mastercode_candidate = tempEditable.toString();
					//((EditText)((AlertDialog)dialog).findViewById(R.id.master_loging)).getText().toString();
				//....
				//마스터 커드와 일치하는 코드가 입력되었는지 확인한다.
				if(SettingManager.master_code.equals(mastercode_candidate))
					//올바른 마스터 코드가 입력되었을 시, 설정 변경 액티비티로 전환
					new Handler().postDelayed(new Runnable() {
						@Override
						public void run() {		
							startActivity(new Intent(MainActivity.this,Setting_Activity.class));
							overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
							finish();
						}
					}, DELAY_TIME);
				else Toast.makeText(getBaseContext(), "마스터 코드 불일치", Toast.LENGTH_SHORT).show();
			}
		});
		
		aDialog.setNegativeButton("취소", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
			}
		});
		
		//대화 상자를 생성
		AlertDialog ad = aDialog.create();
		//대화 상자를 실행
		ad.show();
	}	
	
	//로그를 보기 위한 액티비티로 전환한다.
	public void startLog(View v)throws NameNotFoundException{
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {		
				startActivity(new Intent(MainActivity.this,LogActivity.class));
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				finish();
			}
		}, DELAY_TIME);
	}
}
