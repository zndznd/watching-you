package team.reply1984.sofwareengineering.watchingyou;

import team.reply1984.sofwareengineering.watchingyou.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;

//설정 값들을 변경하기 위한 화면
public class Setting_Activity extends Activity {
	
	static String settingFileName = "setting.output";  //설정 파일의 이름
	protected static final int DELAY_TIME = 100;  	//애니메이션 효과의 값을 설정하기 위한 변수
	protected static SettingManager manager;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setting);
		manager = new SettingManager(this);
		
		EditText input;
		
		//설정 파일이 존재할 시에 설정 파일에 입력되어 있는 값들을 일치하는 입력창에 셋팅해준다.
		input = (EditText)findViewById(R.id.editText_name);
		input.setText(manager.getValue(SettingOption.user_name));
				
		input = (EditText)findViewById(R.id.editText_phone);
		input.setText(manager.getValue(SettingOption.phone));
			
		input = (EditText)findViewById(R.id.editText_master_code);
		input.setText(manager.getValue(SettingOption.master_code));
		       
		input = (EditText)findViewById(R.id.editText_master_code_confirm);
		input.setText(manager.getValue(SettingOption.master_code));
		
		input = (EditText)findViewById(R.id.editText_sender_gmail);
		input.setText(manager.getValue(SettingOption.sender_gmail));
		
		input = (EditText)findViewById(R.id.editText_gmail_password);
		input.setText(manager.getValue(SettingOption.gmail_password));
		
		input = (EditText)findViewById(R.id.editText_watcher_gmail);
		input.setText(manager.getValue(SettingOption.watcher_gmail));
	}
	
	public void saveSettting(View v) throws NameNotFoundException
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(Setting_Activity.this);
		alert.setPositiveButton("확인", new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
		    dialog.dismiss();     //닫기
		    }
		});
		
		String input_name = ((EditText)findViewById(R.id.editText_name)).getText().toString();
		String input_phone = ((EditText)findViewById(R.id.editText_phone)).getText().toString();
		String input_mastercode = ((EditText)findViewById(R.id.editText_master_code)).getText().toString();
		String input_mastercode_confirm = ((EditText)findViewById(R.id.editText_master_code_confirm)).getText().toString();
		String input_sender_gmail = ((EditText)findViewById(R.id.editText_sender_gmail)).getText().toString();
		String input_gmail_password = ((EditText)findViewById(R.id.editText_gmail_password)).getText().toString();
		String input_watcher_gmail = ((EditText)findViewById(R.id.editText_watcher_gmail)).getText().toString();

		//빈 칸이 존재할 시에 경고를 띄우고 함수를 종료한다.
		if((input_name.isEmpty()) ||
				(input_phone.isEmpty()) ||
				(input_sender_gmail.isEmpty()) ||
				(input_gmail_password.isEmpty()) ||
				(input_watcher_gmail.isEmpty()) ||
				(input_mastercode.isEmpty()) ||
				(input_mastercode_confirm .isEmpty())){
			alert.setMessage("모든 사항을 입력해주세요");
			alert.show();
			return;
		}

		//사용자가 마스터 코드를 정확히 설정했는지 확인한다.
		if(input_mastercode.compareTo(input_mastercode_confirm) != 0){
			alert.setMessage("마스터 코드 불일치");
			alert.show();
			return;
		}
		
		//마스터 코드의 길이를 확인한다.
		if(input_mastercode.length()>8 || input_mastercode.length()<1){
			alert.setMessage("비밀번호는 1자리 이상 8자리 이하여야 합니다.");
			alert.show();
			return;
		}
		
		//각 값들을 저장한 후에, 파일로 기록한다.
		manager.setSettingValue(SettingOption.user_name, input_name);
		manager.setSettingValue(SettingOption.phone, input_phone);
		manager.setSettingValue(SettingOption.master_code, input_mastercode);
		manager.setSettingValue(SettingOption.sender_gmail, input_sender_gmail);
		manager.setSettingValue(SettingOption.gmail_password, input_gmail_password);
		manager.setSettingValue(SettingOption.watcher_gmail, input_watcher_gmail);
		manager.saveFile();
		
		//MainActivity로 귀환
		ReturntoMainActivity();
	}
	
	public void closeSetting(View v) throws NameNotFoundException {
		//사용자가 만든 설정 파일이 존재할 시에는 MainActivity로 귀환
		if(manager.getExist()) ReturntoMainActivity();
		//사용자가 설정 파일을 만들지 않았다면 액티비티를 그냥 종료한다.
		else finish();
	}
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
	     case KeyEvent.KEYCODE_BACK:
	    	 if(manager.getExist()) ReturntoMainActivity();
	    	 else finish();
	    break;
		}
    	return true;
    }
	//MainActivity로 전환하는 함수
	public void ReturntoMainActivity(){
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {		
				startActivity(new Intent(Setting_Activity.this,MainActivity.class));
				overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
				finish();
			}
		}, DELAY_TIME);	
	}
}
