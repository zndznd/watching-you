package team.reply1984.sofwareengineering.watchingyou;

import java.io.File;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import android.util.Log;

public class GMailSender
{
	private String mMailHost = "smtp.gmail.com";
	private Session mSession;
	private String user;// = SettingManager.sender_gmail;
	private String pwd;// = SettingManager.gmail_password;
	private String recipients;// = SettingManager.watcher_gmail;

	/* Constructor에 메일을 전송하기 위한 기본 설정 입력 */
	public GMailSender()
	{
		user = SettingManager.sender_gmail;
		pwd = SettingManager.gmail_password;
		recipients = SettingManager.watcher_gmail;
		Properties props = new Properties();
		props.put("mail.transport.protocol", "smtp");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.host", mMailHost);
		props.put("mail.smtp.port", "587");
		mSession = Session
				.getInstance(props, new EmailAuthenticator(user, pwd));
	} // constructor

	/* 파일이 첨부되지 않은 일반 텍스트의 메일을 보내기 위한 함수 설정 */
	public void sendMail(String subject, String body)
	{
		try
		{
			Message msg = new MimeMessage(mSession);
			msg.setFrom(new InternetAddress(user));
			msg.setSubject(subject);
			msg.setContent(body, "text/html;charset=EUC-KR");
			msg.setSentDate(new Date());
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(
					recipients));
			Transport.send(msg);	// 위에서 설정한 내용에 따라 메일 전송
		} catch (Exception e)
		{
			Log.e("lastiverse", "Exception occured : ");
			Log.e("lastiverse", e.toString());
			Log.e("lastiverse", e.getMessage());
		} // try-catch
	} // vodi sendMail

	/* 사진 파일이 첨부된 메일을 보내기 위한 함수 설정 */
	public void sendMailWithFile(String subject, String body, String filePath, String fileName)
	{
		try
		{
			Message msg = new MimeMessage(mSession);
			msg.setFrom(new InternetAddress(user));
			msg.setSubject(subject);
			msg.setSentDate(new Date());
			msg.setRecipient(Message.RecipientType.TO, new InternetAddress(
					recipients));
			
			Multipart multipart=new MimeMultipart();
			
			BodyPart part1 = new MimeBodyPart();	// part1에는 일반 텍스트를 저장
			part1.setText(body);
			
			BodyPart part2 = new MimeBodyPart();	// part2에는 지정된 경로에 저장된 파일을 첨부
			part2.setDataHandler(new DataHandler(new FileDataSource(
					new File(filePath))));
			part2.setFileName(fileName);
			
			multipart.addBodyPart(part1);
			multipart.addBodyPart(part2);

			msg.setContent(multipart);
			
			Transport.send(msg);
			Log.d("lastiverse", "sent");
		} catch (Exception e)
		{
			Log.d("lastiverse", "Exception occured : ");
			Log.d("lastiverse", e.toString());
			Log.d("lastiverse", e.getMessage());
		} // try-catch
	} // void sendMailWithFile

	/* 이메일을 사용하기 위한 사용자 정보 저장 */
	class EmailAuthenticator extends Authenticator
	{
		private String id;
		private String pw;

		public EmailAuthenticator(String id, String pw)
		{
			super();
			this.id = id;
			this.pw = pw;
		} // constructor

		protected PasswordAuthentication getPasswordAuthentication()
		{
			return new PasswordAuthentication(id, pw);
		} // PasswordAuthentication getPasswordAuthentication
	} // class EmailAuthenticator
} // class gmail
