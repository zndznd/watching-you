package team.reply1984.sofwareengineering.watchingyou;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import android.app.Activity;
import android.content.Context;

enum SettingOption{
	user_name,
	phone,
	kakao_id,
	master_code,
	sender_gmail,
	gmail_password,
	watcher_gmail
}

/*
 앱을 사용하기 위한 설정 값들을 관리하고 저장 및 기록하기 위한 클래스이다. 
//*/
public class SettingManager {
	//설정 파일의 이름
	private static final String settingFileName = "setting_output.txt";
	
	
	private boolean is_exist = false; //설정 파일이 존재하는 지의 여부를 기록하는 변수
	public static String name = null; // 사용자의 이름을 저장하는 변수
	public static String phone = null; // 사죵자의 전화 번호를 저장하는 변수
	public static String kakao_id = null; // 사용자의 카카오톡 id를 저장하는 변수
	public static String master_code = null; // 사용자의 마스터 코드를 저장하는 변수
	public static String sender_gmail = null; // 사용자의 지메일 아이디를 저장하는 변수
	public static String gmail_password = null; // 사용자의 지메일 비밀번호를 저장하는 변수
	public static String watcher_gmail = null; // 관찰자의 지메일 아이디를 저장하는 변수
	private Activity activity = null;
	
	//파일을 읽어 들이는 데, 액티비티 클래스의 객체가 필요하기에 기본 생성자는 private으로 설정하여 외부에서의 호출을 막는다.
	@SuppressWarnings("unused")
	private SettingManager(){
	}
	
	public SettingManager(Activity _activity){
		//생성자를 호출함과 동시에 파일을 읽어들인다.
		setActivity(_activity);
		loadFile();
	}
	
	public void setActivity(Activity _activity){
		if(_activity == null)
			return;
		activity = _activity;
	}
	
	//설정 파일을 읽어 들이는 함수
	public boolean loadFile(){
		try{
			FileInputStream fis = activity.openFileInput(settingFileName);			
			BufferedReader buffer = 
				new BufferedReader(new InputStreamReader(fis));
			
			//읽어 들인 설정 파일에 하나라도 빈 것이 있다면 작업을 중단한다.
			if(((name = buffer.readLine()) == null) ||
					((phone = buffer.readLine()) == null) ||
					((kakao_id = buffer.readLine()) == null) ||
					((master_code = buffer.readLine()) == null) ||
					((sender_gmail = buffer.readLine()) == null) ||
					((gmail_password = buffer.readLine()) == null) ||
					((watcher_gmail = buffer.readLine()) == null)){
				fis.close();
				is_exist = false;
			}
			else
				is_exist = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			is_exist = false;
		} catch (IOException e) {
			e.printStackTrace();
			is_exist = false;
		}
		return is_exist;
	}
	
	
	//설정 파일을 쓰기 위한 함수
	public boolean saveFile()
	{
		//설정 파일을 입력하기 위한 모든 값이 셋팅되었는지를 확인한다.
		if((name.isEmpty()) ||
				(phone.isEmpty())||
				(master_code.isEmpty()) ||
				(sender_gmail.isEmpty()) ||
				(gmail_password.isEmpty()) ||
				(watcher_gmail.isEmpty()))
		{
			return false;
		}
		
		try{
			//파일을 쓰기위한 스트림을 생성
			FileOutputStream fos = 
					activity.openFileOutput(settingFileName,Context.MODE_PRIVATE);
			PrintWriter out = new PrintWriter(fos);
			
			//print name
			out.println(name);
			//print phone_number
			out.println(phone);
			//print phone_kakao
			out.println(kakao_id);
			//print phone_master_code
			out.println(master_code);
			//print sender's gmail address
			out.println(sender_gmail);
			//print gmail password
			out.println(gmail_password);
			//print watcher's gmail address
			out.println(watcher_gmail);
			
			out.close();
			is_exist = true;

		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
		
		return true;
	}
	
	//설정 값을 변경하기 위한 함수
	public void setSettingValue(SettingOption option, String value){
		switch(option){
			case user_name:
				name = value;
				break;
			case phone:
				phone = value;
				break;
			case kakao_id:
				kakao_id = value;
				break;
			case master_code:
				master_code = value;
				break;
			case sender_gmail:
				sender_gmail = value;
				break;
			case gmail_password:
				gmail_password = value;
				break;
			case watcher_gmail:
				watcher_gmail = value;
				break;
			default:
		}
	}
	
	//설정 파일의 이름을 얻기 위한 함수
	public String getSettingFileName(){
		return settingFileName;
	}
	
	//설정 값을 읽어 들이기 위한 함수
	public String getValue(SettingOption option){
		if(is_exist == false)
			return null;
		
		switch(option){
			case user_name:
				return name;
			case phone:
				return phone;
			case kakao_id:
				return kakao_id;
			case master_code:
				return master_code;
			case sender_gmail:
				return sender_gmail;
			case gmail_password:
				return gmail_password;
			case watcher_gmail:
				return watcher_gmail;
			default:
				return null;
		}
	}
	
	public boolean getExist()
	{
		return is_exist;
	}
}