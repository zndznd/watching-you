package team.reply1984.sofwareengineering.watchingyou;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.WebView;
import android.widget.EditText;
import android.widget.Toast;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import team.reply1984.sofwareengineering.watchingyou.R;

@SuppressLint({ "SimpleDateFormat", "SdCardPath" })
public class LogActivity extends Activity{
	private WebView mWebView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.log_setting);
		//EditText input;
		//input = (EditText)findViewById(R.id.editYear);
		//String input_name = ((EditText)findViewById(R.id.editText_name)).getText().toString();
	}
	//onClickListener는 public 접근...

	/* 현재 날짜에 저장된 로그를 보기 위한 날짜 형식 */
	public void seeLogToday(View v) {
		String Y = new SimpleDateFormat("yyyy").format(new Date()).toString();
		String M = new SimpleDateFormat("MM").format(new Date()).toString();
		String D = new SimpleDateFormat("dd").format(new Date()).toString();
		seeLog(Y,M,D);
	}

	/* 특정 날짜에 저장된 로그를 보기 위한 날짜 형식 */
	public void seeLogSpecific(View v) {
		String Y = ((EditText)findViewById(R.id.editYear)).getText().toString();
		String M = ((EditText)findViewById(R.id.editMonth)).getText().toString();
		String D = ((EditText)findViewById(R.id.editDay)).getText().toString();
		seeLog(Y,M,D);
	}

	/* 로그 출력하기 */
	private void seeLog(String Y, String M, String D) {
		mWebView = (WebView) findViewById(R.id.webView1);
		mWebView.getSettings().setDefaultTextEncodingName("utf-8");
		File file = new File("/data/data/team.reply1984.sofwareengineering.watchingyou/files/"+Y+M+D+".txt");	// 로그가 기록되는 위치
		if(file.exists()) {
			Toast.makeText(getBaseContext(), Y+"년 "+M+"월 "+D+" 일 로그보기", Toast.LENGTH_SHORT).show();
			mWebView.loadUrl("file:/data/data/team.reply1984.sofwareengineering.watchingyou/files/"+Y+M+D+".txt");
		}
		else {
			Toast.makeText(getBaseContext(), "해당 날짜의 로그가 존재하지 않습니다", Toast.LENGTH_SHORT).show();
			mWebView.clearView();
			mWebView.clearCache(true);
			mWebView.clearHistory();
			mWebView.loadUrl("about:blank");
		}
		//mWebView.loadUrl("file:/storage/sdcard0/Naver/downloadfile-14.jpeg");
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		switch (keyCode) {
	     case KeyEvent.KEYCODE_BACK:
	    	 startActivity(new Intent(LogActivity.this, MainActivity.class));
	    	 overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
	    	 finish();
		}
    	return true;
    }
}